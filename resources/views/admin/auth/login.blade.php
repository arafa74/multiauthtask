@extends('admin.layout.auth')

@section('content')
    @include('admin.layout.change_lang')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('app.login') }}</div>
                <div class="panel-body">
                    <div id="alert-not-found" class="alert alert-danger hide-alert">
                        <ul class="list-unstyled">

                        </ul>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" id="loginForm">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ trans('app.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ trans('app.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('app.remember_me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('app.login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>

        $(document).ready(function () {
            $(".hide-alert").css('display', 'none');

            $("#loginForm").on('submit', function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                e.preventDefault();
                $.ajax({
                    url: '{{ route('adminLogin') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (data) {
                        if ($.isEmptyObject(data.error)) {
                            window.location.href = "{{ route('home')  }}";
                        } else {
                            console.log(data.error);
                            printErrorMsg(data.error);
                        }
                    }
                });

            });

            function printErrorMsg(msg) {
                $(".hide-alert").css('display', 'block');
                $(".hide-alert").find("ul").html('');
                $.each(msg, function (key, value) {
                    $(".hide-alert").find("ul").append('<li>' + value + '</li>');
                });
                $(".hide-alert").delay(3000).fadeOut();
            }
        });
    </script>
    @endsection
