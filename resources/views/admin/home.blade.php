@extends('admin.layout.auth')

@section('content')
    @include('admin.layout.change_lang')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('app.departments') }}</div>

                    <div class="panel-body">
                        <a href="{{ url( LaravelLocalization::getCurrentLocale() .'/admin/departments') }}"> {{ trans('app.show') }} </a>
                    </div>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('app.users') }}</div>

                    <div class="panel-body">
                        <a href="{{ url(LaravelLocalization::getCurrentLocale() .'/admin/users') }}"> {{ trans('app.show') }} </a>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('app.admins') }}</div>

                    <div class="panel-body">
                        <a href="{{ url(LaravelLocalization::getCurrentLocale() .'/admin/admins') }}"> {{ trans('app.show') }} </a>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection
