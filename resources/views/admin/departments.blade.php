@extends('admin.layout.auth')

@section('content')
    @include('admin.layout.change_lang')
    <div class="modal fade" id="modalAddForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">{{ trans('app.add_new_dep') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="alert-not-found" class="alert alert-danger hide-alert ">
                    <ul class="list-unstyled">

                    </ul>
                </div>
                <form id="addDep" method="post">
                    {!! csrf_field() !!}
                    <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            {{--<i class="fa fa-user prefix grey-text"></i>--}}
                            <input type="text" id="orangeForm-name" name="name_ar" class="form-control validate">
                            <label data-error="wrong" data-success="right"
                                   for="orangeForm-name"> {{ trans('app.name_ar') }}</label>
                        </div>
                        <div class="md-form mb-5">
                            {{--<i class="fa fa-envelope prefix grey-text"></i>--}}
                            <input type="text" id="orangeForm-name" name="name_en" class="form-control validate">
                            <label data-error="wrong" data-success="right"
                                   for="orangeForm-email"> {{ trans('app.name_en') }}</label>
                        </div>
                        {{--<div class="md-form mb-5">--}}
                        {{--<select class="form-control">--}}
                        {{--<option>w</option>--}}
                        {{--<option>w</option>--}}
                        {{--<option>w</option>--}}
                        {{--<option>w</option>--}}
                        {{--<option>w</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button type="submit" class="btn btn-deep-orange">{{ trans('app.create') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="text-center">
        <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal"
           data-target="#modalAddForm">{{ trans('app.add_new_dep') }}</a>
    </div>
    </br>
    </br>

    <div class="col-lg-12 col-md-12 col-xs-12 padding table-responsive" id="except_this">
        <form id="update_dep" method="post">
            <fieldset>
            {!! csrf_field() !!}
            <table class="table table-hover table-bordered" id="table-data">
                <thead class="roleListTableHead">
                <tr>
                    <td>#</td>
                    <td>{{ trans('app.name_ar') }}</td>
                    <td>{{ trans('app.name_en') }}</td>
                    <td>{{ trans('app.actions') }}</td>
                </tr>
                </thead>
                @foreach($departments as $department)

                    <tbody class="div_{{ $department->id }}">
                    <tr>
                        <td>
                            {{ $department->id }}
                        </td>
                        <td>
                            <span data-departmentId="{{ $department->id }}">
                            <input type="text" readonly data-id="{{ $department->id  }}" name="name_ar" value="{{ $department->name_ar }}"
                                   class="form-control inline-input">
                            </span>
                        </td>

                        <td>
                            <span data-departmentId="{{ $department->id }}">
                            <input type="text" readonly data-id="{{ $department->id  }}" name="name_en" value="{{ $department->name_en }}"
                                   class="form-control inline-input">
                            </span>
                        <td>
                            <div class="text-left">
                                <span class="btn btn-sm btn-danger remove_dep" data-depId="{{ $department->id }}"
                                      title="حذف"><i class="fa fa-trash-o"></i></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
            </fieldset>
        </form>
    </div>
@endsection
@section('script')

    <script>
        $(document).ready(function () {
            $(".hide-alert").css('display', 'none');

            $("#addDep").on('submit', function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                e.preventDefault();
                $.ajax({
                    url: '{{ route('departments.create') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function (data) {
                        console.log(data);
                        if ($.isEmptyObject(data.error)) {
                            console.log(data.department);
                            $('#table-data').append('<tbody class="div_' + data.department.id + '">\n' +
                                '<tr>\n' +
                                '<td>\n' + data.department.id +
                                '</td>\n' +
                                '<td>\n' +
                                '<input type="text" readonly name="name_ar" value=" ' + data.department.name_ar + '"\n' +
                                '                                   class="form-control inline-input">\n' +
                                '</td>\n' +
                                '<td>\n' +
                                '<input type="text" readonly name="name_en" value="' + data.department.name_en + '"\n' +
                                '                                   class="form-control inline-input"></td>\n' +
                                '<td>\n' +
                                '<div class="text-left">\n' +

                                '<span class="btn btn-sm btn-danger remove_dep" data-depId="' + data.department.id + '"\n' +
                                '                                      title="حذف"><i class="fa fa-trash-o"></i></span>\n' +
                                '</div>\n' +
                                '</td>\n' +
                                '</tr>\n' +
                                '</tbody>');


                            $('#modalAddForm').modal('toggle');
                            $('.modal-backdrop').removeClass("in");
                            $('.modal-backdrop ').hide();
                            swal('{{ trans('app.done_add_dep') }} ', {
                                button: "{{ trans('app.ok') }}"
                            });
                        } else {
                            console.log(data.error);
                            printErrorMsg(data.error);
                        }
                    }
                });

            });

            function printErrorMsg(msg) {
                $(".hide-alert").css('display', 'block');
                $(".hide-alert").find("ul").html('');
                $.each(msg, function (key, value) {
                    $(".hide-alert").find("ul").append('<li>' + value + '</li>');
                });
                $(".hide-alert").delay(3000).fadeOut();
            }
        });


        $('document').ready(function () {
            $('.inline-input').focus(function () {
                $(this).attr('readonly', false)
            });
            $('.inline-input').blur(function (e) {
                $(this).attr('readonly', true);
                var name = $(this).attr("name");
                var value = $(this).val();
                var id = $(this).attr("data-id");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                $.ajax({
                    url: "{{ route('edit.department') }}",
                    type: 'POST',
                    data: "id=" + id + "&name=" + name + "&value=" + value,
                });
            });
        });

        $('document').ready(function () {
            $('.remove_dep').click(function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                var depId = $(this).attr("data-depId");
                swal("{{ trans('app.confirm_delete_dep') }} ", {
                    buttons: {
                        cancel: "{{ trans('app.cancel') }}",
                        catch: {
                            text: "{{ trans('app.ok') }}",
                            value: "true",
                        },
                    },
                }).then((value) => {
                    switch (value) {
                        case "true":
                            $.ajax({
                                method: 'POST',
                                url: "{{ url('/admin/remove_department') }}" + "/" + depId,
                                success: function (data) {
                                    if (data) {
                                        $('.div_' + depId).remove();
                                        swal('{{ trans('app.deleted') }} ', '', 'success', {
                                            button: "{{ trans('app.ok') }}"
                                        });
                                    }
                                },
                            });
                            break;
                    }
                });
            });
        });
    </script>


@endsection