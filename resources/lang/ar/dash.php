<?php

return [

    'rmal_dashboard' => 'لوحة تحكم رمال',

    // auth
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل الخروج',
    'forgot_password' => 'نسيت كلمة المرور',
    
    // general
    'home' => 'الصفحة الرئيسية',
    'settings' => 'الإعدادات',
    'show_data' => 'عرض البيانات',
    'edit_data' => 'تعديل البيانات',
    'delete_data' => 'حذف البيانات',
    'empty' => 'لا يوجد بيانات',
    'soon' => 'قريبا',
    
    // table columns 
    'created_at' => 'أنشئت في',
    'actions' => 'أفعال',
    'name_ar' => 'الاسم باللغة العربية',
    'name_en' => 'الاسم باللغة الانجليزية',
    'image' => 'صورة',    
    'added_successfully' => 'تمت اضافه البيانات بنجاح',    
    'updated_successfully' => 'تم التعديل البيانات بنجاح',    
    
    // action messages
    'deleted_msg_confirm' => 'هل انت متأكد من الحذف ؟',
    'deleted_successfully' => 'تم الحذف بنجاح',
    'try_2_access_not_found_content' => 'محاولة الوصول إلى المحتوى غير موجود',

    // SideBar & NavBar
    'my_account' => 'حسابي',
    'my_profile' => 'الملف الشخصي',
    'main' => 'الرئيسية',
    'others' => 'أخرى',
    'my_profile' => 'صفحتي الشخصية',
    
    // buttons 
    'add_forword_2_menu' => 'إضافة والعودة إلى القائمة',
    'edit_forword_2_menu' => 'تعديل والعودة إلى القائمة',
    'reset_data' => 'تفريغ البيانات',
    'back_to_menu' => 'العودة إلى القائمة',
    
    // Category
    'categories' => 'الأقسام',
    'category' => 'القسم',
    'add_new_category' => 'إضافة قسم جديد',
    'edit_category' => 'تعديل بيانات القسم',
    'ordered_categories' => 'أقسام مرتبة',
    'latest_categories' => 'آخر الاقسام المضافة',    

    // Users
    'add_new_user' => 'إضافة مستخدم جديد',
    'edit_user' => 'تعديل بيانات المستخدم',
    'last_users' => 'آخر المستخدمين المسجلين',
    'users' => 'المستخدمين',
    'users_trashed' => 'المستخدمين في المهملات',
    'user' => 'المستخدم',

    // user form data
    'username' => 'اسم المستخدم',
    'mobile' => 'رقم الجوال',
    'password' => 'كلمة المرور',
    'email' => 'البريد الإلكتروني',
    'profile_image' => 'الصورة الشخصية',
    'status_account' => 'حالة الحساب',
    'ban_reason' => 'سبب الحظر',
    'active' => 'مفعل',
    'deactive' => 'غير مفعل',
    'banned_account' => 'محظور',
    'not_banned_account' => 'غير محظور',

    // Providers
    'add_new_provider' => 'إضافة أسرة جديدة',
    'edit_provider' => 'تعديل بيانات الأسرة',
    'last_providers' => 'آخر الأسر المسجلة',
    'providers' => 'الأسر',
    'providers_trashed' => 'الأسر في المهملات',
    'provider' => 'الأسرة',
    'provider_data' => 'بيانات الأسرة',
    'provider_products' => 'منتجات الأسرة',
    'store_name' => 'اسم الأسرة',
    'delivery_price' => 'سعر التوصيل',
    'number_civil_registry' => 'رقم السجل المدني',
    'minimum_charge' => 'الحد الأدنى للطلب',
    
    // Cities
    'add_new_city' => 'اضف مدينة جديدة',
    'cities' => 'المدن',
    'city' => 'المدينة',
    'edit_city' => 'تعديل المدينة',
    'latest_cities' => 'احدث المدن',
    'zip_code' => 'الرمز البريدي',
    'select_place_on_map' => 'اختر المكان على الخريطة',
    'lat' => 'خط العرض',
    'lng' => 'خط الطول',
    
    // Countries
    'country' => 'الدولة',
    'add_new_country' => 'اضف دولة جديدة',
    'edit_country' => 'تعديل الدولة',
    'countries' => 'الدول',
    'choose_country' => 'يجب عليك اختيار الدولة',
    'latest_countries' => 'احدث الدول',
    'nationality_name_ar' => 'اسم الجنسية بالعربية',
    'nationality_name_en' => 'اسم الجنسية بالانجليزية',
    'phonecode' => 'مفتاح الدولة',
    
    // Products
    'product' => 'المنتج',
    'add_new_product' => 'أضف منتج جديد',
    'edit_product' => 'تعديل المنتج',
    'products' => 'المنتجات',
    'latest_products' => 'أحدث المنتجات',
    'name' => 'الإسم',
    'price' => 'السعر',
    'offer' => 'عرض',
    'offer_price' => 'سعر العرض',
    'has_offer' => 'لديه عرض',
    'has_not_offer' => 'لا يحتوي على عرض',
    'latest_products' => 'أحدث المنتجات',
    'description' => 'الوصف',
    'images' => 'الصور',

    // administration
    'administration' => ' الادارة ',
    'administration_groups' => ' مجموعات الادارة ',
    'add_new_administration_groups' => ' اضف مجموعه ادارية جديدة ',
    'create' => ' اضافة ',
    'trash' => ' سلة المهملات ',
    'edit' => ' تعديل ',

    // error pages
    '401' => " لا تملك الصلاحية للوصول الى الصفحة الحالية ",
    '404' => " هذه الصفحة غير موجوده ",

    'copy_write' => 'جميع الحقوق محفوظه لمؤسسة رمال الأودية © ' . date('Y') . '.',
    'developed_by' => 'تم برمجته بواسطة : م.عبدالله عرابي',

];
