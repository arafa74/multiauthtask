<?php

return [

    'dashboard' => 'الصفحه الرئيسيه',
    'show' => 'عرض',
    'add_new_dep' => 'اضافه قسم جديد',
    'users' => 'الاعضاء',
    'departments' => 'الاقسام',
    'admins' => 'المشرفين',
    'login' => 'تسجيل الدخول',
    'remember_me' => 'تذكرنى',
    'email' => 'البريد الالكترونى',
    'password' => 'كلمه المرور',
    'create' => 'اضافه',
    'name_ar' => 'الاسم بالعربيه ',
    'name_en' => 'الاسم بالانجليزيه',
    'done_add_dep' => 'تم اضافه القسم بنجاح',
    'ok' => 'موافق',
    'error_data' => 'عفوا خطا فى بيانات الدخول',
    'actions' => 'الاعدادات',
    'confirm_delete_dep' => 'هل انت متاكد من حذف القسم ؟',
    'cancel' => 'الغاء',
    'name' => 'الاسم',
    'dep' => 'القسم',
    'add_new_user' => 'اضافه عضو جديد',
    'confirm_delete_user' => 'هل انت متاكد من حذف العضو ؟',
    'done_add_user' => 'تم اضافه العضو بنجاح',
    'add_new_admin' => 'اضافه مشرف جديد',
    'confirm_delete_admin' => 'هل انت متاكد من حذف المشرف ؟',
    'done_add_admin' => 'تم اضافه المشرف بنجاح',
    'deleted' => 'تم الحذف بنجاح',

];
