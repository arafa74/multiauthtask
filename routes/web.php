<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin']], function () {
        Route::get('/home', function () {
            return view('admin.home');
        })->name('home');
        Route::get('departments', 'DepartmentController@show')->name('departments');
        Route::post('departments/create', 'DepartmentController@create')->name('departments.create');
        Route::post('/remove_department/{id}','DepartmentController@remove')->name('remove_department');
        Route::post('/edit_department','DepartmentController@edit')->name('edit.department');
//        ------------------------------------------------------------------------------

        Route::get('users', 'UserController@show')->name('users');
        Route::post('users/create', 'UserController@create')->name('users.create');
        Route::post('/remove_user/{id}','UserController@remove')->name('remove_user');
        Route::post('/edit_user','UserController@edit')->name('edit.user');
//        -------------------------------------------------------------------------------
        Route::get('admins', 'AdminController@show')->name('admins');
        Route::post('admins/create', 'AdminController@create')->name('admins.create');
        Route::post('/remove_admin/{id}','AdminController@remove')->name('remove_admin');
        Route::post('/edit_admin','AdminController@edit')->name('edit.admin');
//       --------------------------------------------------------------------------------
    });

    Route::group(['prefix' => 'admin', 'namespace' => 'AdminAuth'], function () {
        Route::get('/login', 'LoginController@showLoginForm')->name('admin_login');
//        Route::post('/login', 'LoginController@login');
        Route::post('/login', 'LoginController@adminLogin')->name('adminLogin');
        Route::post('/admin_logout', 'LoginController@logout')->name('admin_logout');
    });
});