<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use Validator;
use App\Http\Requests\AddDep;

class DepartmentController extends Controller
{
    public function show()
    {
        $data['departments'] = Department::all();
        return view('admin.departments', $data);
    }

    public function create(AddDep $request)
    {
        $department = Department::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);
        return response()->json(['status' => 1, 'department' => $department]);
    }

    public function remove($id)
    {
        if ($department = Department::find($id)) {

            $department->delete();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function edit(Request $request)
    {
        $department = Department::find($request->id);
            if ($request->name == 'name_ar'){
                $department->name_ar = $request->value;
            }
            if ($request->name == 'name_en'){
                $department->name_en = $request->value;
            }
             $department->update();
            return response()->json(['status' => true , 'data'=> $department]);
    }

}
