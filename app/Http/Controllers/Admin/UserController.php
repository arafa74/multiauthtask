<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddUser;

class UserController extends Controller
{
    public function show()
    {
        $data['departments'] = Department::all();
        $data['users'] = User::all();
        return view('admin.users', $data);
    }

    public function create(AddUser $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'department_id' => $request->department_id,
        ]);
        return response()->json(['status' => 1, 'user' => $user]);
    }

    public function remove($id)
    {
        if ($user = User::find($id)) {

            $user->delete();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function edit(Request $request)
    {
        $user = User::find($request->id);
        if ($request->name == 'name'){
            $user->name = $request->value;
        }
        if ($request->name == 'email'){
            $user->email = $request->value;
        }
        $user->update();
        return response()->json(['status' => true , 'data'=> $user]);
    }
}
