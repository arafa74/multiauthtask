<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Requests\AddAdmin;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function show()
    {
        $data['admins'] = Admin::all();
        return view('admin.admins', $data);
    }

    public function create(AddAdmin $request)
    {
        $admin = Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        return response()->json(['status' => 1, 'admin' => $admin]);
    }

    public function remove($id)
    {
        if ($admin = Admin::find($id)) {

            $admin->delete();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function edit(Request $request)
    {
        $admin = Admin::find($request->id);
        if ($request->name == 'name'){
            $admin->name = $request->value;
        }
        if ($request->name == 'email'){
            $admin->email = $request->value;
        }
        $admin->update();
        return response()->json(['status' => true , 'data'=> $admin]);
    }
}
